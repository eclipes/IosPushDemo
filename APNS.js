const apn = require("apn");

let users = [
{ name: "John",  "devices": ["5d2010c21697b5095d13336ebe9937de81ad4b3a3ec03f4867cbac32cfe7270d"]},
];

let service = new apn.Provider({
	cert: "./apns/apns-dev-cert.pem",
	key: "./apns/apns-dev-key.pem",
});

users.forEach( (user) => {

	let note = new apn.Notification();
	// note.alert = `Hey ${user.name}, I just sent my first Push Notification`;

  // The topic is usually the bundle identifier of your application.
  note.topic = "com.nino.art";
  note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
  note.badge = 9;
  // note.sound = "ping.aiff";
  note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
  note.payload = {'messageFrom': 'John Appleseed'};

  console.log(`Sending: ${note.compile()} to ${user.devices}`);

  service.send(note, user.devices).then( result => {
  	console.log("sent:", result.sent.length);
  	console.log("failed:", result.failed.length);
  	console.log(result.failed);
  });
});

// For one-shot notification tasks you may wish to shutdown the connection
// after everything is sent, but only call shutdown if you need your
// application to terminate.
service.shutdown();

// let tokens = ["5d2010c21697b5095d13336ebe9937de81ad4b3a3ec03f4867cbac32cfe7270d"];

// let service = new apn.Provider({
//  cert: "./apns/apns-dev-cert.pem",
//   key: "./apns/apns-dev-key.pem",
// });

// let note = new apn.Notification({
// 	alert:  "Breaking News: I just sent my first Push Notification",
// });

// // The topic is usually the bundle identifier of your application.
// note.topic = "com.nino.art";

// console.log(`Sending: ${note.compile()} to ${tokens}`);
// service.send(note, tokens).then( result => {
//     console.log("sent:", result.sent.length);
//     console.log("failed:", result.failed.length);
//     console.log(result.failed);
// });


// For one-shot notification tasks you may wish to shutdown the connection
// after everything is sent, but only call shutdown if you need your 
// application to terminate.
service.shutdown();